﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    // This is the invoker class in the command pattern
    // Command history of PhoneBook operations
    // We need to keep two stacks: done and undone
    // This is also where do, undo and redo are implemented.
    class History
    {
        private Stack<Command> done, undone;

        public History()
        {
            done = new Stack<Command>();
            undone = new Stack<Command>();
        }

        // execute command and store it on the done commands list;
        //   old undone commands are permanently deleted
        public void Do(Command new_cmd)
        {
            new_cmd.execute();
            undone.Clear();
            done.Push(new_cmd);
        }

        // undoes last executed command; precondition: at least one command to undo
        public void Undo()
        {
            // To do: finish undo steps
        }

        // redoes last undone command; precondition: at least one command to redo
        public void Redo()
        {
            // To do: finish redo steps
        }

    }
}
