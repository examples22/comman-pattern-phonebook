﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        private Handler handler;

        public Form1()
        {
            InitializeComponent();
        }

        private void Show_Data(List<Entry> contacts)
        {
            //populate data source
            BindingSource source = new BindingSource();
            for (int i = 0; i < contacts.Count; i++)
                source.Add(contacts[i]);

            //initialize datagridview
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.AutoSize = true;
            dataGridView1.DataSource = source;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            handler = new Handler();
            handler.PrePopulateBook();
            Show_Data(handler.Phonebook.Book);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            handler.Undo();
            Show_Data(handler.Phonebook.Book);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            handler.Redo();
            Show_Data(handler.Phonebook.Book);
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            handler.AddEntry(textBox_FirstName.Text, textBox_LastName.Text, textBox_PhoneNumber.Text);
            Show_Data(handler.Phonebook.Book);
        }

        private void button_Remove_Click(object sender, EventArgs e)
        {
            handler.RemoveEntry(textBox_PhoneNumber.Text);
            Show_Data(handler.Phonebook.Book);
        }

        private void dataGridView1_SelectionChanged_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                textBox_FirstName.Text = row.Cells[0].Value.ToString();
                textBox_LastName.Text = row.Cells[1].Value.ToString();
                textBox_PhoneNumber.Text = row.Cells[2].Value.ToString();
            }
        }
    }
}
