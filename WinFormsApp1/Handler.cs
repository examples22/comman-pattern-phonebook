﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    // this is the Client class
    // where you instantiate the Receiver and concrete commands
    // in this program, it also serves as a boundary class interfacing with UI.
    class Handler
    {
        // Invoker
        public History com_history;
        // Receiver
        public PhoneBook Phonebook { get; set; }

        public Handler()
        {
            Phonebook = new PhoneBook();
            com_history = new History();
        }
        public void AddEntry(string fn, string ln, string phone)
        {
            // To do: ask com_history to DO AddCommand. Need to instantiate a new AddCommand object.
            
        }
        public void RemoveEntry(string phone)
        {
            // To do: as com_history to DO RemoveCommand. Need to instantiate a new RemoveCommand object.
        }
        public void Undo()
        {
            com_history.Undo();
        }
        public void Redo()
        {
            com_history.Redo();
        }

        // only for demo purpose
        public void PrePopulateBook()
        {
            Phonebook.Add(new Entry("Tony", "Stark", "1212"));
            Phonebook.Add(new Entry("Thor", "Odinson", "2589"));
            Phonebook.Add(new Entry("Hentry", "Pym", "3482"));
            Phonebook.Add(new Entry("Robert", "Banner", "1479"));
            Phonebook.Add(new Entry("Wanda", "Maximoff", "9513"));
        }
    }
}
