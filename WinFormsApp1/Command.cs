﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    abstract class Command
    {
        protected PhoneBook phonebook;
        public Command(PhoneBook pb) { phonebook = pb; }
        public abstract void execute();
        public abstract void unexecute();
    }

    // wrap adding a new entry to the phonebook as a command
    // hint: you should keep a record of what was added.
    class AddCommand : Command
    {
        private Entry entry_to_add;
        public AddCommand(PhoneBook pb, Entry e) : base(pb)
        {
            entry_to_add = e;
        }
        public override void execute()
        {
           // To do: receiver.action()
        }
        public override void unexecute()
        {
            // To do: receiver.action() 
        }

    }

    // wrap removing an entry from the phonebook as a command
    // hint: you should keep a record of what was removed.
    class RemoveCommand : Command
    {
        private Entry entry_to_remove;
        public RemoveCommand(PhoneBook pb, Entry e) : base(pb)
        {
            entry_to_remove = e;
        }
        public override void execute()
        {
            // To do: receiver.action()
        }
        public override void unexecute()
        {
            // To do: receiver.action()
        }
    }
}
