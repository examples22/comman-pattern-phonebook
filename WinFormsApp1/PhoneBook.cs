﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    // single entry
    class Entry
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public Entry(string fn= "", string ln = "", string p = "")
        {
            this.FirstName = fn;
            this.LastName = ln;
            this.Phone = p;
        }
    }

    // collection of phone book entries
    class PhoneBook
    {
        public List<Entry> Book { get; set; }

        public PhoneBook()
        {
            Book = new List<Entry>();
        }

        // find an entry in the phonebook by phone number
        public Entry Find(string phone)
        {
            for (int i = 0; i < Book.Count(); i++)
            {
                if (phone == Book[i].Phone)
                    return Book[i];
            }
            return null;
        }

        // find an entry in the phonebook by name
        public Entry Find(string fn, string ln)
        {
            for (int i = 0; i < Book.Count(); i++)
            {
                if (fn == Book[i].FirstName && ln == Book[i].LastName)
                    return Book[i];
            }
            return null;
        }

        public void Add(Entry new_entry)
        {
            Book.Add(new_entry);
        }

        // remove an entry from the phonebook by name
        public void Remove(string fn, string ln)
        {
            Entry entry_to_remove = Find(fn, ln);
            if (entry_to_remove != null)
                Book.Remove(entry_to_remove);
        }

        // remove an entry from the phonebook by phone number
        public void Remove(string phone)
        {
            Entry entry_to_remove = Find(phone);
            if (entry_to_remove != null)
                Book.Remove(entry_to_remove);
        }
    }
}
